jQuery(document).ready(function () {
    var formObj = jQuery('form#donate-form');
    
    formObj.validate({
        submitHandler: function(form) {
            var alertObj = $('#alert-message');
            var overLayObj = $('.overlay-form');
            jQuery.ajax({
                url: siteUrl + "libs/ShipService_v15_php/php/ShipWebServiceClient/Express/Domestic/ShipWebServiceClient.php",
                //url: siteUrl + "libs/OpenShipWebServiceClient.php",
                type: 'POST',
                data: {
                    dataForm : formObj.serialize()
                },
                //dataType : "json",
                beforeSend :function(){
                    overLayObj.fadeIn();
                },
                success: function (data) {
                    overLayObj.fadeOut();
                   alertObj.find('.alert-content').html(data).fadeIn();
                     alertObj.fadeIn();
                }
            });
        }
    });

     $('.alert-close').click(function(){
        var obj = $(this);
        obj.closest('.alert').fadeOut().find('.alert-content').html('');
    });
});
